variable "config" {
  type = object({
    rg_name  = string
    location = string
  })
}

variable "environment" {
  type = string
}

# variable "subscription_id" {
#   type = string
# }
# variable "client_id" {
#   type = string
# }
# variable "client_secret" {
#   type = string
# }
# variable "tenant_id" {
#   type = string
# }
