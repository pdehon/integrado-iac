resource "azurerm_storage_account" "sa" {
  name                     = "saintegrado"
  resource_group_name      = var.config.rg_name
  location                 = var.config.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}
