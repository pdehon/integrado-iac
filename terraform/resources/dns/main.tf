resource "azurerm_resource_group" "example" {
  name     = "domain-example-rg"
  location = var.config.location
}

resource "azurerm_dns_zone" "example" {
  name                = "mydomain.com"
  resource_group_name = var.config.rg_name
}

resource "azurerm_dns_a_record" "example" {
  name                = "test"
  zone_name           = azurerm_dns_zone.example.name
  resource_group_name = azurerm_resource_group.example.name
  ttl                 = 300
  records             = ["10.0.180.17"]
}
