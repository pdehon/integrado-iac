resource "azurerm_container_registry" "acr" {
  name                = "acr-integrado"
  resource_group_name = var.config.rg_name
  location            = var.config.location
  sku                 = "Basic"
  admin_enabled       = false
}