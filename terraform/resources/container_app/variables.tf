variable "config" {
  type = object({
    rg_name  = string
    location = string
  })
}

variable "environment" {
  type = string
}