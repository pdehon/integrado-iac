data "azurerm_log_analytics_workspace" "law" {
  name                = "law-integrado"
  resource_group_name = var.config.rg_name
}

resource "azurerm_container_app_environment" "cae" {
  name                       = "cae-integrado"
  location                   = var.config.location
  resource_group_name        = var.config.rg_name
  log_analytics_workspace_id = data.azurerm_log_analytics_workspace.law.id
}

resource "azurerm_container_app" "ca" {
  name                         = "ca-integrado"
  container_app_environment_id = azurerm_container_app_environment.cae.id
  resource_group_name          = var.config.rg_name
  revision_mode                = "Single"

  template {
    container {
      name   = "integradocontainerapp"
      image  = "pdehon/integrado:latest"
      cpu    = 0.25
      memory = "0.5Gi"
    }
  }

  ingress {
    external_enabled = true    
    target_port = 80
    transport = "auto"
    traffic_weight {
        percentage = 100
        latest_revision = true        
    }
  }

}

resource "azurerm_container_app" "caapp" {
  name                         = "ca-integrado-app"
  container_app_environment_id = azurerm_container_app_environment.cae.id
  resource_group_name          = var.config.rg_name
  revision_mode                = "Single"

  template {
    container {
      name   = "integradocontainerapp.aplicativo"
      image  = "pdehon/integrado_app:latest"
      cpu    = 0.25
      memory = "0.5Gi"
    }
  }

  ingress {
    external_enabled = true    
    target_port = 80
    transport = "auto"
    traffic_weight {
        percentage = 100
        latest_revision = true        
    }
  }

}