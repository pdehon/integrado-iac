resource "azurerm_resource_group" "rg" {
  name     = var.config.rg_name
  location = var.config.location
}
