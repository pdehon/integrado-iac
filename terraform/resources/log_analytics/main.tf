resource "azurerm_log_analytics_workspace" "law" {
  name                = "law-integrado"
  location            = var.config.location
  resource_group_name = var.config.rg_name
  sku                 = "PerGB2018"
  retention_in_days   = 30
}

output "log_analytics_id" {
  value = azurerm_log_analytics_workspace.law.id
}