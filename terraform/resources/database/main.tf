resource "azurerm_mssql_server" "ams" {
  name                         = "db-integrado-dev"
  resource_group_name          = var.config.rg_name
  location                     = var.config.location
  version                      = "12.0"
  administrator_login          = "4dm1n157r470r"
  administrator_login_password = "4-v3ry-53cr37-p455w0rd"
}

resource "azurerm_mssql_database" "test" {
  name         = "DBintegrado"
  server_id    = azurerm_mssql_server.ams.id
  collation    = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  sku_name     = "S0"
}
