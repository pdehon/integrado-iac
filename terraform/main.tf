module "resource_group" {
  source      = "./resources/resource_group"
  config      = var.config
  environment = var.environment
}

module "storage_account" {
  source      = "./resources/storage_account"
  config      = var.config
  environment = var.environment
  depends_on  = [module.resource_group]
}

module "database" {
  source      = "./resources/database"
  config      = var.config
  environment = var.environment
  depends_on  = [module.resource_group]
}

module "log_analytics" {
  source      = "./resources/log_analytics"
  config      = var.config
  environment = var.environment
  depends_on  = [module.resource_group]
}

module "container_app" {
  source      = "./resources/container_app"
  config      = var.config
  environment = var.environment
  depends_on  = [module.log_analytics]
}

# module "container_registry" {
#   source      = "./resources/container_registry"
#   config      = var.config
#   environment = var.environment
#   depends_on  = [module.resource_group]
# }

# depende de um domínio registrado
# module "dns" {
#   source      = "./resources/dns"
#   config      = var.config
#   environment = var.environment
#   depends_on  = [module.resource_group]
# }
