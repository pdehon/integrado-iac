terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.60.0"
    }
  }
  # backend "azurerm" {
  #   storage_account_name = "NOME_DA_CONTA_DE_ARMACENAMENTO"
  #   container_name       = "NOME_DO_CONTAINER"
  #   key                  = "NOME_DA_CHAVE"
  # }
}